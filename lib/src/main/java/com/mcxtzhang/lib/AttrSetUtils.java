/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mcxtzhang.lib;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;

public class AttrSetUtils {

    public static int optInt(AttrSet attrSet,
                              String key,
                              int def) {
        if (attrSet == null) {
            return def;
        }
        return attrSet.getAttr(key).isPresent()
                ? attrSet.getAttr(key).get().getIntegerValue() : def;
    }

    public static String optString(AttrSet attrSet,
                                   String key,
                                   String def) {
        if (attrSet == null) {
            return def;
        }
        return attrSet.getAttr(key).isPresent()
                ? attrSet.getAttr(key).get().getStringValue() : def;
    }

    public static float optPixelSize(AttrSet attrSet,
                                      String key,
                                      float def) {
        if (attrSet == null) {
            return def;
        }

        return attrSet.getAttr(key).isPresent()
                ? attrSet.getAttr(key).get().getDimensionValue() : def;
    }

    public static int optPixelSize(AttrSet attrSet,
                                    String key,
                                    int def) {
        if (attrSet == null) {
            return def;
        }

        return attrSet.getAttr(key).isPresent()
                ? attrSet.getAttr(key).get().getDimensionValue() : def;
    }

    public static int optColor(AttrSet attrSet,
                                String key,
                                int def) {
        if (attrSet == null) {
            return def;
        }
        return attrSet.getAttr(key).isPresent()
                ? attrSet.getAttr(key).get().getColorValue().getValue() : def;
    }

    public static boolean optBoolean(AttrSet attrSet,
                                      String key,
                                      boolean def) {
        if (attrSet == null) {
            return def;
        }
        return attrSet.getAttr(key).isPresent()
                ? attrSet.getAttr(key).get().getBoolValue() : def;
    }

    public static Element optElement(AttrSet attrSet,
                                     String key,
                                     Element def) {
        if (attrSet == null) {
            return def;
        }
        return attrSet.getAttr(key).isPresent()
                ? attrSet.getAttr(key).get().getElement() : def;
    }

}
