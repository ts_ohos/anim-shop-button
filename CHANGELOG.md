# Changelog
本组件基于原库master版本移植(8286b43 on 21 Mar 2017 Git stats)

## v.1.0.0
移植后首次发布上传
## v.1.0.1
完善了按钮伸缩位移旋转的动画效果

## support
原库UI样式基本全部移植

## not support
鸿蒙目前无法实现多类型item类似网格的效果，导致按钮’列表中使用‘中的切换按钮没有效果