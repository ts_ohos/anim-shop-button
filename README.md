# AnimShopButton
本项目是基于开源项目AnimShopButton进行harmonyos化的移植和开发的，可以通过项目标签以及
[github地址](https://github.com/mcxtzhang/AnimShopButton).


移植版本：源master版本

## 项目介绍
### 项目名称：AnimShopButton
### 所属系列：harmonyos的第三方组件适配移植
### 功能：
    一个仿饿了么 带伸缩位移旋转动画的购物车按钮


### 项目移植状态：完全移植
### 项目移植问题：该项目在使用Region类中的setPath方法时发现，该方法在API5和API6中获取到的值是存在差异，请将API升级到6使用。
### 调用差异：基本没有使用差异，请参照demo使用
### 原项目Doc地址：https://github.com/mcxtzhang/AnimShopButton
### 编程语言：java

### 项目demo截图

![鸿蒙运行效果](art/demo.gif)  
![鸿蒙运行效果](art/screenPicture.png)

## 安装教程

#### 方案一
建议下载开源代码并参照demo引入相关库：
```groovy
 dependencies {
    implementation project(':lib')
 }  
```
    
#### 方案二
* 项目根目录的build.gradle中的repositories添加：
```groovy
  buildscript {
       repositories {
           ...
           mavenCentral()
       }
       ...
   }
   
   allprojects {
       repositories {
           ...
           mavenCentral()
       }
   }
```
* module目录的build.gralde中dependencies添加：
```groovy
dependencies {
     implementation 'com.gitee.ts_ohos:AnimShopButton:1.0.1'
 }
```

## 使用说明

* 属性介绍

|name|format|description|中文解释
|:---:|:---:|:---:|:---:|
| isAddFillMode| boolean| Plus button is opened Fill mode default is stroke (false)|加按钮是否开启fill模式 默认是stroke(false)
| addEnableBgColor| color|The background color of the plus button|加按钮的背景色
| addEnableFgColor| color|The foreground color of the plus button|加按钮的前景色
| addDisableBgColor| color|The background color when the button is not available|加按钮不可用时的背景色
| addDisableFgColor| color |The foreground color when the button is not available|加按钮不可用时的前景色
| isDelFillMode| boolean| Plus button is opened Fill mode default is stroke (false)|减按钮是否开启fill模式 默认是stroke(false)
| delEnableBgColor| color|The background color of the minus button|减按钮的背景色
| delEnableFgColor| color|The foreground color of the minus button|减按钮的前景色
| delDisableBgColor| color|The background color when the button is not available|减按钮不可用时的背景色
| delDisableFgColor| color |The foreground color when the button is not available|减按钮不可用时的前景色
| radius| dimension|The radius of the circle|圆的半径
| circleStrokeWidth| dimension|The width of the circle|圆圈的宽度
| lineWidth| dimension|The width of the line (+ - sign)|线(+ - 符号)的宽度
| gapBetweenCircle| dimension| The spacing between two circles|两个圆之间的间距
| numTextSize| dimension| The textSize of draws the number|绘制数量的textSize
| maxCount| integer| max count|最大数量
| count| integer| current count|当前数量
| hintText| string| The hint text when number is 0|数量为0时，hint文字
| hintBgColor| color| The hint background when number is 0|数量为0时，hint背景色
| hintFgColor| color| The hint foreground when number is 0|数量为0时，hint前景色
| hingTextSize| dimension| The hint text size when number is 0|数量为0时，hint文字大小
| hintBgRoundValue| dimension| The background fillet value when number is 0|数量为0时，hint背景圆角值
| ignoreHintArea| boolean| The UI/animation whether ignores the hint area|UI显示、动画是否忽略hint收缩区域
| perAnimDuration| integer| The duration of each animation, in ms|每一段动画的执行时间，单位ms
| hintText| string| The hint text when number is 0|数量为0时，hint文字
| replenishTextColor| color| TextColor in replenish status|补货中状态的文字颜色
| replenishTextSize| dimension| TextSize in replenish status|补货中状态的文字大小
| replenishText| string | Text hint in replenish status|补货中状态的文字
* 功能具体实现请根据 demo （entry）部分实现构建

## 例子

* XML实现

```xml
    <!--使用默认UI属性-->
    <com.mcxtzhang.lib.AnimShopButton
            ohos:id="$+id:btn1"
            ohos:width="match_content"
            ohos:height="match_content"
            app:maxCount="3" />
    <!--设置了两圆间距-->
    <com.mcxtzhang.lib.AnimShopButton
            ohos:id="$+id:btn2"
            ohos:width="match_content"
            ohos:height="match_content"
            app:count="3"
            app:gapBetweenCircle="90vp"
            app:hintFgColor="#ffffff"
            app:hintText="小马快到碗里来"
            app:maxCount="99"
            app:perAnimDuration="200" />
    <!--仿饿了么-->
    <com.mcxtzhang.lib.AnimShopButton
            ohos:id="$+id:btnEleList"
            ohos:width="match_content"
            ohos:height="match_content"
            app:addEnableBgColor="#3190E8"
            app:addEnableFgColor="#ffffff"
            app:hintBgColor="#3190E8"
            app:hintBgRoundValue="15vp"
            app:hintFgColor="#ffffff"
            app:ignoreHintArea="true"
            app:maxCount="99" />
```

License
--------
                                Apache License
                           Version 2.0, January 2004
                        http://www.apache.org/licenses/
    https://gitee.com/ts_ohos/anim-shop-button/blob/master/LICENSE