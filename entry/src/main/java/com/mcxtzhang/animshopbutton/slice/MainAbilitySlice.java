package com.mcxtzhang.animshopbutton.slice;

import com.mcxtzhang.animshopbutton.AddDelViewDemoAbility;
import com.mcxtzhang.animshopbutton.ResourceTable;
import com.mcxtzhang.animshopbutton.TestAttrAbility;
import com.mcxtzhang.animshopbutton.utils.AbilityToAbility;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_launcher);
        findComponentById(ResourceTable.Id_btnAttr)
                .setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                AbilityToAbility.toAbility(
                        MainAbilitySlice.this, TestAttrAbility.class);
            }
        });

        findComponentById(ResourceTable.Id_btnRv)
                .setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                AbilityToAbility.toAbility(
                        MainAbilitySlice.this, AddDelViewDemoAbility.class);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
