package com.mcxtzhang.animshopbutton.utils;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.aafwk.content.Operation;
import ohos.app.Context;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AbilityToAbility {

    ////////////////////////////////////////普通Activity跳转////////////////////////////////////////

    /**
     * 普通Ability之间跳转
     * @param ability
     * @param abilityName 目标ability name
     */
    public static void toAbility(Context ability, String abilityName) {
        toAbilityForResult(ability, abilityName, null, 0);
    }

    public static void toAbility(Context ability, Class toAbility) {
        toAbilityForResult(ability, toAbility, null, 0);
    }

    /**
     * 普通Ability之间跳转
     *
     * @param ability
     * @param abilityName 目标ability name
     * @param params 携带参数
     */
    public static void toAbility(Context ability, String abilityName, Map<String, ?> params) {
        toAbilityForResult(ability, abilityName, params, 0);
    }

    public static void toAbility(Context ability, Class toAbility, Map<String, ?> params) {
        toAbilityForResult(ability, toAbility, params, 0);
    }

    /**
     * 普通Ability之间跳转
     *
     * @param ability
     * @param abilityName 目标ability name
     * @param requestCode 请求码  需大于0
     */
    public static void toAbilityForResult(Context ability, String abilityName, int requestCode) {
        toAbilityForResult(ability, abilityName, null, requestCode);
    }

    public static void toAbilityForResult(Context context,
                                          String abilityName,
                                          Map<String, ?> params,
                                          int requestCode) {

        Intent intent = new Intent();

        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(context.getBundleName())
                .withAbilityName(abilityName)
                .build();

        intent.setOperation(operation);
        assembleParams(intent, params);
        context.startAbility(intent, requestCode);
    }

    public static void toAbilityForResult(Context context,
                                          Class ability,
                                          Map<String, ?> params,
                                          int requestCode) {

        Intent intent = new Intent();

        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(context.getBundleName())
                .withAbilityName(ability)
                .build();

        intent.setOperation(operation);
        assembleParams(intent, params);
        context.startAbility(intent, requestCode);
    }

    private static void assembleParams(Intent intent, Map<String, ?> params) {
        if (params != null) {
            for (Map.Entry<String, ?> entry : params.entrySet()) {
                String key = entry.getKey();
                Object value = params.get(key);
                if (value instanceof String) {
                    intent.setParam(key, (String) value);
                } else if (value instanceof Boolean) {
                    intent.setParam(key, (boolean) value);
                } else if (value instanceof Integer) {
                    intent.setParam(key, (int) value);
                } else if (value instanceof Float) {
                    intent.setParam(key, (float) value);
                } else if (value instanceof Double) {
                    intent.setParam(key, (double) value);
                } else if (value instanceof Long) {
                    intent.setParam(key, (long) value);
                } else if (value instanceof Short) {
                    intent.setParam(key, (short) value);
                } else if (value instanceof IntentParams) {
                    intent.setParam(key, (IntentParams) value);
                } else if (value instanceof ArrayList) {
                    intent.setParam(key, (ArrayList) value);
                } else if (value instanceof HashMap) {
                    intent.setParam(key, (HashMap) value);
                }
            }
        }
    }

}

