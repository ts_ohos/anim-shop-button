package com.mcxtzhang.animshopbutton;

import com.mcxtzhang.lib.AnimShopButton;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class TestAttrAbility extends Ability {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_test_attr);

        final AnimShopButton animShopButton =
                (AnimShopButton) findComponentById(ResourceTable.Id_btnReplenish);
        animShopButton.setReplenish(true);
    }
}
