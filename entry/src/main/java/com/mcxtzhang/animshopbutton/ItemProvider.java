/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mcxtzhang.animshopbutton;

import com.mcxtzhang.lib.AnimShopButton;
import com.mcxtzhang.lib.IOnAddDelListener;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;
import java.util.List;

public class ItemProvider extends BaseItemProvider {

    private List<AddDelBean> list;
    private Ability ability;

    public ItemProvider(List<AddDelBean> list, Ability ability) {
        this.list = list;
        this.ability = ability;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component,
                                  ComponentContainer componentContainer) {
        component = LayoutScatter.getInstance(ability)
                .parse(ResourceTable.Layout_item_add_del, null, false);
        Text text = (Text) component.findComponentById(ResourceTable.Id_tv);
        text.setText(list.get(i).getName());
        AnimShopButton animShopButton = (AnimShopButton)
                component.findComponentById(ResourceTable.Id_addDelView);
        AddDelBean data = list.get(i);
        animShopButton.setCount(data.getCount());
        animShopButton.setMaxCount(data.getMaxCount());
        animShopButton.setOnAddDelListener(new IOnAddDelListener() {
            @Override
            public void onAddSuccess(int count) {
                data.setCount(count);
            }

            @Override
            public void onAddFailed(int count, FailType failType) {

            }

            @Override
            public void onDelSuccess(int count) {
                data.setCount(count);
            }

            @Override
            public void onDelFaild(int count, FailType failType) {

            }
        });
        animShopButton.setReplenish(data.isReplenish());
        return component;
    }
}
