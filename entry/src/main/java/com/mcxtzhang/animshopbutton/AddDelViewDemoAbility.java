package com.mcxtzhang.animshopbutton;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.window.dialog.ToastDialog;

import java.util.ArrayList;
import java.util.List;

public class AddDelViewDemoAbility extends Ability {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_add_del_view_demo);
        final ListContainer rv = (ListContainer) findComponentById(ResourceTable.Id_rv);
        findComponentById(ResourceTable.Id_btnChange)
                .setClickedListener(new Component.ClickedListener() {
            int i = 0;

            @Override
            public void onClick(Component component) {
//                if ((i++ & 1) == 1) {
//                    rv.setLayoutManager(new DirectionalLayoutManager());
//                } else {
//                    TableLayoutManager tableLayoutManager = new TableLayoutManager();
//                    tableLayoutManager.setColumnCount(2);
//                    rv.setLayoutManager(tableLayoutManager);
//                }
                new ToastDialog(AddDelViewDemoAbility.this)
                        .setText("鸿蒙暂不支持该样式切换")
                        .setDuration(500)
                        .show();
            }
        });

        ItemProvider itemProvider = new ItemProvider(getDatas(), this);
        rv.setItemProvider(itemProvider);
    }


    public List<AddDelBean> getDatas() {
        List<AddDelBean> result = new ArrayList<>();
/*        result.add(new AddDelBean(5, 1));
        result.add(new AddDelBean(10, 1));

        result.add(new AddDelBean(1, 1));
        result.add(new AddDelBean(0, 0));

        result.add(new AddDelBean(4, 2));*/

        for (int i = 0; i < 10; i++) {
            result.add(new AddDelBean(i, 10, (((i&1) == 0) ? true : false)));
        }

        return result;
    }
}
